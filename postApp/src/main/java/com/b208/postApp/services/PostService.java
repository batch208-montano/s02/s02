package com.b208.postApp.services;

import com.b208.postApp.models.Post;

public interface PostService {

    //methods to manipulate database
    void createPost(Post post);
    Iterable<Post> getPosts();
    void updatePost(Long id, Post post);

    void deletePost(Long id);

}
